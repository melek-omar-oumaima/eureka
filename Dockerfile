# Use an official OpenJDK runtime as a parent image
FROM openjdk:11-jre-slim

# Set the working directory to /app
WORKDIR /app


COPY target/eureka-server-0.0.1-SNAPSHOT.jar  /app/eureka-server-0.0.1-SNAPSHOT.jar

# Make port 8761 available to the world outside this container
EXPOSE 8761

# Define environment variable
ENV SERVICE_NAME eureka

# Run application when the container launches
CMD ["java", "-jar", "eureka-server-0.0.1-SNAPSHOT.jar"]







